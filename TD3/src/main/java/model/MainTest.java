package model;

public class MainTest {
    public static void main(String[] args) {
        User admin = new User("Rich", "Cis", "asd@go.co", "1234");
        admin.setAdmin(true);
        User u1 = new User("user1", "hello", "asd@ad.co", "123");

        User test1 = admin.addUser("test1", "hello", "asd@ad.co", "123");
        User test2 = u1.addUser("test2", "hello", "asd@ad.co", "123");

        System.out.println(test1.getFirstName());
        System.out.println(test2.getLastName());

    }
}

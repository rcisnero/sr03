package model;

import java.sql.Time;
import java.util.Date;

public class Chat {
    private Date creationDate;
    private Time validationTime;
    private int id;
    private String title;
    private String description;
    private User owner;

    public Chat(Date creationDate, Time validationTime, String title, String description, User owner) {
        this.creationDate = creationDate;
        this.validationTime = validationTime;
        this.title = title;
        this.description = description;
        this.owner = owner;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Time getValidationTime() {
        return validationTime;
    }

    public void setValidationTime(Time validationTime) {
        this.validationTime = validationTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

package model;

import java.util.Date;

public class Message {
    private int idMessage;
    private String content;
    private Chat parentChat;
    private Date creationDate;
    private User userOwner;

    public int getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(int idMessage) {
        this.idMessage = idMessage;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Chat getParentChat() {
        return parentChat;
    }

    public void setParentChat(Chat parentChat) {
        this.parentChat = parentChat;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public User getUserOwner() {
        return userOwner;
    }

    public void setUserOwner(User userOwner) {
        this.userOwner = userOwner;
    }

}

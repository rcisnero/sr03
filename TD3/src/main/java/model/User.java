package model;

import java.util.ArrayList;

public class User {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private boolean admin;

    private ArrayList<Chat> invitedChats;

    public User(String firstName, String lastName, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public void createChat(){

    }

    public User addUser(String firstName, String lastName, String email, String password){
        if (this.isAdmin()) {
            return new User(firstName, lastName, email, password);
        }
        else {
            System.out.println("Operation non permis");
            return null;
        }
    }

    public void deleteUser(){

    }

    public void deactivateUser(){

    }
}
